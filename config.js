var devPaths = {
  bowerFolder: 'bower_components/',
  allCss: 'src/scss/bower.scss',
  scss: 'src/scss/',
  css: 'src/css/',
  scripts: 'src/js/',
  images: 'src/images/',
  fonts: 'src/fonts/',
  html: 'template-parts/',
  footerFolder: '',
  footerTpl: 'footer.php'
}
var distPaths = {
  root: 'dist/',
  css: 'dist/css/',
  scripts: 'dist/js/',
  images: 'dist/images/',
  fonts: 'dist/fonts/',
  html: 'dist/',
  footerFolder: 'dist/'
}
var bowerBootstrapSrc = 'bower_components/bootstrap/js/dist/'
var bowerBootstrapDist = 'bower_components/bootstrap/dist/js/'
var bootstrapComponents = [
  bowerBootstrapSrc + 'util.js',
  bowerBootstrapSrc + 'alert.js',
  // bowerBootstrapSrc + 'button.js',
  // bowerBootstrapSrc + 'carousel.js',
  bowerBootstrapSrc + 'collapse.js',
  bowerBootstrapSrc + 'dropdown.js',
  bowerBootstrapSrc + 'modal.js',
  // bowerBootstrapSrc + 'popover.js',
  // bowerBootstrapSrc + 'scrollspy.js',
  // bowerBootstrapSrc + 'tab.js',
  // bowerBootstrapSrc + 'tooltip.js'
]
var purifyCssSrc = [
  devPaths.html + '**/*.tpl',
  distPaths.scripts + '**/*.js'
]

var settingsAutoprefixer = {
  browsers: [
    'last 2 versions', 
    'android 4'
  ]
}

var settingsFonts = '**/*.{woff,woff2}'

module.exports = {
    devPaths: devPaths,
    distPaths: distPaths,
    bowerBootstrapSrc: bowerBootstrapSrc,
    bowerBootstrapDist: bowerBootstrapDist,
    bootstrapComponents: bootstrapComponents,
    purifyCssSrc: purifyCssSrc,
    settingsAutoprefixer: settingsAutoprefixer,
    settingsFonts: settingsFonts
}


var devUrl = 'http://dev.blog.cfolio.net/'
var devWiredepUrl = '/wp-content/themes/CF_blog/src/js/'