var autoprefixer = require('gulp-autoprefixer')
var browserSync = require('browser-sync')
var changed = require('gulp-changed')
var concat = require('gulp-concat')
var cssnano = require('gulp-cssnano')
var del = require('del')
var gulp = require('gulp')
var gulpIf = require('gulp-if')
var imagemin = require('gulp-imagemin')
var imageminJpegRecompress = require('imagemin-jpeg-recompress')
var purifycss = require('gulp-purifycss')
var runSequence = require('run-sequence')
var sass = require('gulp-sass')
var sourcemaps = require('gulp-sourcemaps')
var uglify = require('gulp-uglify')
var useref = require('gulp-useref')
var wiredep = require('wiredep').stream
var critical = require('critical')
// custom modules
var config = require('./config.js')
var flags = {
  production: false
}

// Development Tasks 
// -----------------
// Start browserSync server
gulp.task('browserSync', function() {
  browserSync.init({
    files: ['{template-parts}/**/*.php', '*.php'],
    proxy: config.devUrl,
    snippetOptions: {
      whitelist: ['/wp-admin/admin-ajax.php'],
      blacklist: ['/wp-admin/**']
    }
  })
})
// Sass convert
gulp.task('sass', function() {
  return gulp.src(config.devPaths.scss + '**/*.scss')
    .pipe(gulpIf(!flags.production, sourcemaps.init()))
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({ browsers: config.settingsAutoprefixer.browsers }))
    .pipe(gulpIf(!flags.production, sourcemaps.write()))
    .pipe(gulpIf(flags.production, cssnano()))
    .pipe(changed(config.devPaths.css, { hasChanged: changed.compareSha1Digest }))
    .pipe(gulp.dest(config.devPaths.css))
    .pipe(browserSync.reload({
      stream: true
    }))
})
// Automatically inject Less and Sass Bower dependencies
gulp.task('bowerStyles', function () {
  return gulp.src(config.devPaths.allCss)
    .pipe(wiredep())
    .pipe(gulp.dest(config.devPaths.scss))
})
// Automatically inject js
gulp.task('bowerScripts', function () {
  return gulp.src(config.devPaths.footerTpl)
    .pipe(wiredep({
      fileTypes: {
        html: {
          replace: {
            js: '<script src="/wp-content/themes/CF_blog/{{filePath}}"></script>',
            css: '<link rel="stylesheet" href="/wp-content/themes/CF_blog/{{filePath}}" />'
          }
        }
      },
      exclude: [
        '/wp-content/themes/CF_blog/bower_components/tether/dist/js/tether.js',
        '/wp-content/themes/CF_blog/bower_components/bootstrap/dist/js/bootstrap.js'
      ]
    }))
    .pipe(gulp.dest(config.devPaths.footerFolder))
})
// Copy-paste fontawesome
gulp.task('fontAwesome', function() {
  return gulp.src(config.devPaths.bowerFolder + 'font-awesome/fonts/' + config.settingsFonts)
    .pipe(gulp.dest(config.devPaths.fonts))
})
// Bower tasks
gulp.task('bower', function(callback) {
  runSequence('bowerStyles', 'bowerScripts', 'fontAwesome',
    callback
  )
})
// Watchers
gulp.task('watch', function() {
  gulp.watch('*.css', ['sass'])
  gulp.watch(config.devPaths.scss + '**/*.scss', ['sass'])
  gulp.watch(config.devPaths.scripts + '**/*.js', browserSync.reload)
  gulp.watch(config.devPaths.html + '**/*.html', browserSync.reload)
  gulp.watch(['bower.json'], ['bower'])
})


// Production Tasks
// -----------------
// Clean before production
gulp.task('clean:dist', function() {
  return del.sync(config.distPaths.root)
})
// Create custom bootstrap.js
gulp.task('customBootstrap', function() {
  return gulp.src(config.bootstrapComponents)
    .pipe(concat('bootstrap.js'))
    .pipe(uglify())
    .pipe(gulp.dest(config.bowerBootstrapDist))
})
// Contcatenation scripts
gulp.task('useref', function() {
  return gulp.src(config.devPaths.footerTpl)
    .pipe(useref({
        transformPath: function(filePath) {
            return filePath.replace('/wp-content/themes/CF_blog/','')
        }
    }))
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulp.dest(config.distPaths.footerFolder))
})
// Remove unnecessary css
gulp.task('removeUnnecessaryCss', function() {
  return gulp.src(config.distPaths.css + '**/*.css')
    .pipe(purifycss(config.purifyCssSrc))
    .pipe(cssnano())
    .pipe(gulp.dest(config.distPaths.css))
})
// Optimizing Images 
gulp.task('images', function() {
  return gulp.src(config.devPaths.images + '*')
    .pipe(imagemin([
      imagemin.gifsicle(),
      imageminJpegRecompress({
        loops:4,
        min: 50,
        max: 95,
        quality:'high' 
      }),
      imagemin.optipng(),
      imagemin.svgo()
    ]))
    .pipe(gulp.dest(config.distPaths.images))
})
// Copy-paste fonts
gulp.task('fonts', function() {
  return gulp.src(config.devPaths.fonts + config.settingsFonts)
  .pipe(gulp.dest(config.distPaths.fonts))
})
// critical css
gulp.task('critical', function (cb) {
    critical.generate({
        src: 'http://dev.blog.cfolio.net/',
        css: [
          'src/css/bower.css',
          'src/css/style.css'
        ],
        dest: 'dist/inline_style.css',
        minify: true,
        width: 1300,
        height: 650
    });
});


//Default task - dev
gulp.task('default', function(callback) {
  runSequence(
    'bower',
    'sass',
    'browserSync',
    ['watch'],
    callback
  )
})
gulp.task('build', function(callback) {
  flags.production = true
  runSequence(
    'clean:dist',
    'sass',
    // 'customBootstrap',
    ['useref', 'images', 'fonts'],
    // 'removeUnnecessaryCss',
    'critical',
    callback
  )
})